# Napiste funkci, ktera na vstupu vezme retezec, napr. 'The quick Brow Fox' a udela soucet velkych a malych pismen v retezci.
# Tyto dve hodnoty vrati
# pouzijte funkce isupper() a islower()

def pismena (retezec):
    velka = 0
    mala = 0
    for x in retezec:
        if x.isupper():
            velka += 1
        if x.islower():
            mala += 1
    return velka, mala

print (pismena('The quick Brown Fox'))