a=0; b=10               # definovat promenne muze i na jednom radku
while a < b:
    print(a, end=' ')
    a += 1              # stejny zapis jako a = a + 1
# vysledek bude 0 1 2 3 4 5 6 7 8 9
