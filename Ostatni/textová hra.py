"""
Vaším úkolem je vytvořit textovou hru:
https://cs.wikipedia.org/wiki/Textov%C3%A1_hra

Při tvorbě použijte proměnné, funkci input(), funkci print(), alespoň 4 podmínky (jedna z podmínek bude
obsahovat i elif větev a zanořenou podmínku včetně operatorů and a or).

Můžete udělat hru na jakékoliv téma. Hlavně ať to není nudné a splní to minimální podmínky zadání!

Celkem to bude za 5 bodů. Dalších 5 bonusových bodů (nezapočítávají se do základu)
můžete získat za správné použití 5 libovolných funkcí pro řětězce, čísla (např. split() apod.). Další funkce zkuste vygooglit na internetu.
Použité funkce se musí vztahovat k logice hry.
"""



print('Textová hra - Závod v orientačním běhu')
print (' ')
age = input('Zadej svůj věk pro určení kategorie: ')
print (' ')
if age <'12':
    print ('kategorie: žactvo')
elif age >= '12' and age < '18':
    print ('kategorie: dorost')
elif age >= '18' and age < '30':
    print ('kategorie: dospělí')
elif age >= '30':
    print('kategorie: veteráni')

print (' ')
print ('Nacházíš se na startu závodu o 5 kontrolách. Při cestě na první kontrolu se nabízí dvě varianty postupu.')
print ('První vede po cestě a je delší, druhá přímo terénem, ale je kratší. Kterou variantu zvolíš?')
print (' ')

postup_jedna = input('volím postup číslo: ')
if postup_jedna == '1':
    print('Horší varianta, ztratil jsi 2 minuty')
    ztráta_1 = 2
else:
    print('Skvělá volba, minuta náskok')
    ztráta_1 = -1
print (' ')
print ('Na další kontrolu jsou také dvě varianty postupu, zvolíš variantu 1 nebo 2?')
print (' ')

postup_dvě = input ('tvoje varianta: ')
print (' ')
if postup_dvě == '1':
    print ('Vyvrtnul sis kotník, nemůžeš běžet, ztráta bude + 10 minut!!!')
    ztráta_2 = 10
else:
    print ('Správná volba, pokračuješ rychle dále a získáváš minutu k dobru')
    ztráta_2 = -1
print (' ')

print ('Postup z třetí kontroly je fyzicky náročný a závody jsou i o štěstí.')
print ('Vygenerované číslo ti ukáže jak dobře ti dnes běží nohy. 1 je nejlepší, 5 nejhorší.')
print (' ')

import random
x = random.randint(1,5)
print('Tvé nohy dnes běží na úrovni: ',x)
while x == 1:
    print ('Cítíš se skvěle, vítězství je blízko!')
    ztráta_3 = -10
    break

while x == 2:
    print('Není to vůbec špatné, bedna je možná!')
    ztráta_3 = -5
    break

while x == 3:
    print ('Mohlo by to být lepší, ale bude to fajn výsledek!')
    break

while x == 4:
    print ('Dneska ses špatně vyspal!')
    ztráta_3 = +5
    break

while x == 5:
    print ('Závod je v háji')
    ztráta_3 = +5
    break
print (' ')

print ('Do cíle zbývá kousek a žene tě chuť na pivo. Závody se konají ve městě, kde se vyrábí pivo Hostan.')
print ('Pokud napíšeš o které město se jedná, získáš bonus 2 minuty, pokud ne 4 minuty ztratíš!!!')
print (' ')

tip = input ('Město, kde se vyrábí pivo Hostan je: ')
if tip == 'Znojmo':
    print ('Správně, jsi v cíli a dáváš si oroseného Hostana')
    ztráta_4 = -2
else:
    print ('Špatně, je to Znojmo, žízeň asi není tak velká, ztrácíš')
    ztráta_4 = +4

print (' ')
ztráta = ztráta_1 + ztráta_2 + ztráta_3 + ztráta_4
if ztráta  < 0:
    print('VÍTĚZSTVÍ s náskokem: ', ztráta,' ','minut')
else:
    print ('Dnes jsi poražen se ztrátou: ', ztráta,' ', 'minut')
print(' ')

print('DÍKY ZA HRU!!!')
print ('David Chovanec')