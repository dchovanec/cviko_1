# Vytvorte si seznam, ktere bude uvnitr obsahovat nekolik slovniku.
# Kazdy slovnik bude obsahovat 3 klice a 3 hodnoty (brand => Ford, engine => 1.0, price => 300000, percent=0.4).
# Takhle vytvorte alespon 3 slovniky se tremi druhy aut a rozdilnymi hodnotami.
# Pote iterujte prvni seznamem a uvnitr cyklu iterujte klicem i hodnotou jednotlivych slovniku.
# Pro kazdy automobil zvyšte cenu o procento, ktere se nachazi pod klicem percent
# Vysledne slovniky vypiste se zmenenymi hodnotami

seznam = []

slovnik1 = {'brand': 'škoda', 'engine': 1.9, 'price': 150900, 'percent': 1.5}
slovnik2 = {'brand': 'volkswagen', 'engine': 2.5, 'price': 1500000, 'percent': 0.5}
slovnik3 = {'brand': 'toyota', 'engine': 1.6, 'price': 50000, 'percent': 1.9}

seznam.append (slovnik1)
seznam.append (slovnik2)
seznam.append (slovnik3)

for x in seznam:
   print (x)
   print (x['price'])
   print(x['percent'])
   x['price'] = x['price'] + x['price']*x['percent']/100
   print(x['price'])