"""
Resenim teto pisemky bude zpracovat 3 dopisy (letter1, letter2 a letter3) od 3 deti, ktere pisou jeziskovi a nakoupit veci z dopisu.
V kazdem dopise je objekt, ktery je mozne koupit na eshopu (promenna stores).
Cilem je nakoupit jeziskovi v eshopech vsechny veci, o ktere si deti napsaly.
Jezisek ma omezeny rozpocet (promenna money), ale muze si vzit uver od banky (promenna bank).
Projdete dopisy deti a najdete v dopisech veci, ktere mate koupit. Veci jsou oddelene - (pomlckou) a jsou vzdy na konci vet.
V dopisech od deti jsou gramaticke chyby, takze bude potreba hledat varianty s i/y nebo ceske prepisy (pro plejstejsn budete hledat playstation).
Tyto veci se pote pokuste najit v eshopech a "koupit". Pri koupeni musite odecist jednu polozku z poctu dostupnych polozek na eshopu a odecist penize jeziska.
Preferujte nejlevnejsi moznost.
Pokud jezisek nebude mit na danou vec dost penez, bude si muset pujcit od banky (odectete penize z banky).
Nakonec programu vytvorite formatovany vypis do konzole, ktery bude obsahovat informace o vsech nakoupenych polozkach (obchod, cena).
Dale bude obsahovat kolik jeziskovy zbylo penez a kolik si pujcil od banky a o kolik vic zaplati na urocich.

SHRNUTI:
1. Zpracujete dopisy od deti (zohlednite gramaticke chyby a ceske tvary)
2. Vyhledate darky z dopisu na eshopu
3. Koupite nejlevnejsi darky na eshopech a odectete pocet polozek v obchode
4. Pokud jeziskovi nevyjdou penize tak si pujcite od banky
5. Vypisete informace o nakupech a jeziskovych financich

ZPUSOB IMPLEMENTACE JE NA VAS. SAMOTNE RESENI (FUNKCE) BUDETE MUSET VYMYSLET SAMI.
"""

bank = 1000000
rate = 19.9
money = 30000

letter1 = """Myly jezisku, na vanoce bych si pral -hodinky s vodotriskem"""
letter2 = """Na vanoce bich moc chtel -plejstejsn"""
letter3 = """Na vanoce bych si prala -ponika"""

stores = [
    {'name': 'Alza', 'products': [
        {'name': 'hodinky s vodotryskem', 'price': 150, 'no_items': 5},
        {'name': 'pocitac', 'price': 20000, 'no_items': 2},
        {'name': 'ponik', 'price': 50000, 'no_items': 1}
    ]},
    {'name': 'CZC', 'products': [
        {'name': 'playstation', 'price': 9000, 'no_items': 5},
        {'name': 'Dobroty Ladi Hrusky', 'price': 10, 'no_items': 100},
        {'name': 'ponik', 'price': 70000, 'no_items': 1}
    ]}
]

ceny_ponik = []
ceny_hodinky = []
ceny_PS = []

for store in stores:
    'eshop: {}'.format(store['name'])
    for product in store ['products']:
        if product['name'] == 'ponik':
            ceny_ponik.append (product['price'])
            print('ponik min cena:', min (ceny_ponik))


for store in stores:
    'eshop: {}'.format(store['name'])
    for product in store ['products']:
        if product['name'] == 'hodinky s vodotryskem':
            ceny_hodinky.append(product['price'])
            print('hodinky min cena:', min (ceny_hodinky))

for store in stores:
    'eshop: {}'.format(store['name'])
    for product in store ['products']:
        if product['name'] == 'playstation':
            ceny_PS.append(product['price'])
            print('PS min cena:', min (ceny_PS))

cena1 = min (ceny_ponik)
cena2 = min (ceny_hodinky)
cena3 = min (ceny_PS)
suma = cena1 + cena2 + cena3

print ('celkem za darky:', suma)

def odecist (castka_za_darky):
    zustane = money - castka_za_darky
    return zustane

print (odecist(59150))



nakoupene_darky = []


