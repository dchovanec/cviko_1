class Car:
    def __init__(self, vykon, max_rychlost, znacka, barva):
        self.vykon = vykon
        self.max_rychlost = max_rychlost
        self.znacka = znacka
        self.barva = barva

    def nastartuj(self):
        print ('Nastartoval auto značky: {}'.format(self.znacka))

car = Car(81, 250, 'Skoda', 'modrá')
print (car.vykon)
print(car.nastartuj())
car2 = Car(91, 200, 'Toyota', 'zelená')
print(car2.nastartuj())

