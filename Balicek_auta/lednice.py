from Balicek_auta.food import Food

class Fridge:

    def __init__(self, brand):
        self.brand = brand
        self.place = []

    def get_count(self):
        return len(self.place)

    def add_food(self, food):
        if food.expiration is False:
            self.place.append (food)
            return True
        return False

food = Food ('syr', 100, True)
fridge = Fridge('Gorenje')
fridge.add_food(food)