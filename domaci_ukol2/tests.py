import unittest
from .fridge import *


class TestFridge(unittest.TestCase):

    def test_add_food(self):
        self.assertTrue(add_food({'name': 'blue cheese', 'price': 40, 'expired': False}))
        self.assertFalse(add_food({'name': 'blue cheese', 'price': 40, 'expired': True}))

    def test_add_foods(self):
        self.assertTrue(add_foods({'name': 'ham', 'price': 30, 'expired': False},
                                           {'name': 'salad', 'price': 45, 'expired': False},
                                           {'name': 'egg', 'price': 4, 'expired': False},
                                           {'name': 'wine', 'price': 200, 'expired': False},
                                           {'name': 'orange juice', 'price': 40, 'expired': False}))